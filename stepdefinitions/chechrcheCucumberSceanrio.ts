import { browser, ExpectedConditions as ec,element,by } from "protractor";
import { GoogleSearchPage } from "../pages/googleSearchPageObject";
const { Given, When, Then } = require("cucumber");
const chai = require("chai").use(require("chai-as-promised"));
const expect = chai.expect;

let googleSearchPage : GoogleSearchPage ;

Given(/^the user has access to google search page$/, async () => {
  const pageTitle = await browser.getTitle();
  expect(pageTitle).to.eq('Google')
});

Given(/^the user set cucumber in search input$/, async () => {
    googleSearchPage = new GoogleSearchPage();
    await googleSearchPage.setSearchInput('cucumber');
    const inputValue = await googleSearchPage.getSearcheInputValue();
    expect(inputValue).to.eq('cucumber');
});

When(/^the user clicks on Google Search button$/, async () => {
    googleSearchPage = new GoogleSearchPage();
    await googleSearchPage.clickOnGoogleSearchButton();
  
});

Then(/^the user can see the url of cucumber site$/, async () => {
    googleSearchPage = new GoogleSearchPage();
   const cucumberSiteLink =  await googleSearchPage.getOncucumberSiteLink();
   expect(cucumberSiteLink).to.eq('https://cucumber.io/');
});




