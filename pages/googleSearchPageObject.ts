import { element, by, ElementFinder, browser,ExpectedConditions as ec } from 'protractor';
export class GoogleSearchPage{
   searchInput = element(by.name('q'));
   googleSearchButton = element(by.name('btnK'));
  cucumberSiteLink = element(by.xpath('//*[@id="rso"]/div[1]/div/div/div/div[1]/a'));
  async setSearchInput(searched){
      await this.searchInput.clear();
      await this.searchInput.sendKeys(searched);
  }

  async clickOnGoogleSearchButton(){
      await this.googleSearchButton.click();
  }

  async getOncucumberSiteLink (){
      return this.cucumberSiteLink.getAttribute('href');
  }
  
  async getSearcheInputValue(){
      return this.searchInput.getAttribute('value');
  }

}