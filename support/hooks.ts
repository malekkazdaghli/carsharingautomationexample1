const { BeforeAll, After, AfterAll, Status,setDefaultTimeout } = require("cucumber");
import { browser } from "protractor";

setDefaultTimeout(100 * 1000);
BeforeAll({timeout: 100 * 1000}, async () => {
    await browser.get('/'); // get app url 
    browser.waitForAngularEnabled(false)  
});
 // after evry scenario we take screenshot fro evry failed ,ambiguous and skipped scenarios
After(async function(scenario) {
    if ((scenario.result.status === Status.FAILED) || (scenario.result.status === Status.AMBIGUOUS) ||(scenario.result.status === Status.SKIPPED)  ) {
         const screenShot = await browser.takeScreenshot();
         this.attach(screenShot, "image/png");
    }
});

// after all senario we need to close the browser.
AfterAll({timeout: 100 * 1000}, async () => {
await browser.quit();
});
