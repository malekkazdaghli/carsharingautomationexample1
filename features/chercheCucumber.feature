Feature: cherche cucumber in google 
Scenario: as a user I can find cucumber site link in google 
Given the user has access to google search page
And the user set cucumber in search input 
When the user clicks on Google Search button 
Then the user can see the url of cucumber site 
