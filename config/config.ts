import * as path from "path";
import { browser, Config } from "protractor";

const jsonReports = process.cwd() + "/reports/json";

export const config: Config = {
    allScriptsTimeout: 50000, // all script time out 
    directConnect: false,  // true to local selenuim grid local browser; false to specifier seluim grid server 
    seleniumAddress: "http://127.0.0.1:4444/wd/hub", // adress of selenuim grid server 
    baseUrl: "https://www.google.it/",  // your app start url 
    capabilities: {     // browser and system capabilites 
        browserName: "chrome",
        chromeOptions: {
            args: ['disable-infobars']
        },
        metadata: {
            browser: {
                name: 'chrome',
                version: '58'
            },
            device: 'Dell insperot',
            platform: {
                name: 'ubuntu',
                version: '18.04'
            }
        },
        
    },
    framework: "custom", // for cucumber 
    frameworkPath: require.resolve("protractor-cucumber-framework"), // path of cucumber framework 
    suites: { // test suite of features 
        search: ["../../features/chercheCucumber.feature"],
       // logout: ["./e2e/features/account/evallogout.feature"],
    },
    cucumberOpts: { // cucumber options
        compiler: "ts:ts-node/register", 
        format: "json:build/../../report/json/cucumber_report.json",
        require: ["/home/mortadha/Desktop/cucumber/Documentation Test/Web/CucumberModel/typeScript/stepdefinitions",  // step definition required
           
            "../../support/hooks.ts"],
        strict: true,
    },

    plugins: [{  // plugins for cucumber html report 
        package: 'protractor-multiple-cucumber-html-reporter-plugin',
        options: {
            // read the options part for more options
            automaticallyGenerateReport: true,
            removeExistingJsonReportFile: true,
            displayDuration: true,
            openReportInBrowser : true,
            reportPath :"../../report/html",
            reportName : "Carsharing End2End Test Report",
            pageTitle :"Carsharing End2End Test Report",
            pageFooter : "<div align='center'>© 2018 machinestalk. All Rights Reserved</div>",
            customData: {
                title: 'Run info',
                data: [
                    {label: 'Project', value: 'Carsharing'},
                    {label: 'Execution Start Time', value: new Date()}
                ]
            }
        }
    }],
    beforeLaunch: function () {
        require('ts-node').register({
            project: ''
        });

    },


    onPrepare: function () {   
        browser.ignoreSynchronization = true; // because using ng prime we need ignored the angular synchronization 
        browser.driver.manage().window().maximize();   // browser size 
        browser.executeScript('document.body.className += "notransition";'); 
    },

}